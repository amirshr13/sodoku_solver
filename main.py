sudoku_grid6= [[5, 3, 0, 0, 7, 0, 0, 0, 0],
               [6, 0, 0, 1, 9, 5, 0, 0, 0],
               [0, 9, 8, 0, 0, 0, 0, 6, 0],
               [8, 0, 0, 0, 6, 0, 0, 0, 3],
               [4, 0, 0, 8, 0, 3, 0, 0, 1],
               [7, 0, 0, 0, 2, 0, 0, 0, 6],
               [0, 6, 0, 0, 0, 0, 2, 8, 0],
               [0, 0, 0, 4, 1, 9, 0, 0, 0],
               [0, 0, 0, 0, 8, 0, 0, 7, 9]]

sudoku_grid = [[3, 8, 5, 1, 0, 2, 0, 7, 0],
                [0, 0, 4, 0, 0, 8, 2, 0, 0],
                [0, 0, 0, 0, 0, 0, 8, 3, 5],
                [5, 0, 8, 4, 0, 0, 0, 1, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 9, 0, 0, 0, 0, 0, 5, 0],
                [0, 0, 0, 8, 7, 1, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 6, 0, 0],
                [8, 4, 0, 2, 5, 0, 1, 9, 7]]


"""
  The objective is to fill a 9×9 grid with digits
  so that each column, each row,
  and each of the nine 3×3 subgrids that
  compose the grid contain all of the digits from 1 to 9
"""


class BruteForce:

    def __init__(self):
        self.sudoku_grid = None
        self.sudoku_solutions = set()
        self.sudoku_grid_tuple = None

    def read_grid(self, grid):
        self.sudoku_grid = grid

    def is_possible(self, number, row, column):
        sudoku_grid = self.sudoku_grid

        def check_in_row(row, number):
            # This method checks if a number is valid in a specific row
            for i in range(0, 9):
                if sudoku_grid[row][i] == number:
                    return False
            return True

        def check_in_column(column, number):
            # This method checks if a number is valid in a specific column
            for i in range(0, 9):
                if sudoku_grid[i][column] == number:
                    return False
            return True

        def check_in_square(row, column, number):
            # check if its a valid square in sudoku or not
            row_start = (row // 3) * 3
            col_start = (column // 3) * 3
            for i in range(row_start, row_start + 3):
                for j in range(col_start, col_start + 3):
                    if sudoku_grid[i][j] == number:
                        return False
            return True

        if check_in_column(column, number) and check_in_row(row, number) and check_in_square(row, column, number):
            return True
        return False

    def dont_have_empty_cell(self):
        # checks if we have empty cells in our grid
        for row in range(0, 9):
            for col in range(0, 9):
                if self.sudoku_grid[row][col] == 0:
                    empty_cell = (row, col)
                    return False, empty_cell
        return True, 1

    def save_solutions(self):
        grid = self.change_grid_to_tuple()
        self.sudoku_solutions.add(grid)

    def change_grid_to_tuple(self):
        rows_list = []
        for index, row in enumerate(self.sudoku_grid):
            rows_list.append(tuple(row))
        self.sudoku_grid_tuple = tuple(rows_list)
        return self.sudoku_grid_tuple

    def start(self):
        if self.dont_have_empty_cell()[0]:
            return True
        row = self.dont_have_empty_cell()[1][0]
        column = self.dont_have_empty_cell()[1][1]
        for num in range(1, 10):

            if self.is_possible(num, row, column):
                self.sudoku_grid[row][column] = num
                self.change_grid_to_tuple()

                if self.start() and self.sudoku_grid_tuple not in self.sudoku_solutions:
                    self.save_solutions()
                    return True
                self.sudoku_grid[row][column] = 0
        return False

    def print_solutions(self):
        for sol in self.sudoku_solutions:
            for i in sol:
                print(i)
            print('\n')
        print(f'number of answers: {len(self.sudoku_solutions)}')


if __name__ == '__main__':
    b = BruteForce()
    b.read_grid(sudoku_grid)
    b.start()
    b.print_solutions()
